"""
Copyright (c) 2022 Aurore Guillevic
MIT License

Freeman Scott Teske constructions in https://eprint.iacr.org/2006/372
k=9,15,27 constructions implemented in https://eprint.iacr.org/2016/1187
"""

from sage.all_cmdline import *   # import sage library

from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.arith.functions import lcm
from sage.arith.misc import gcd, xgcd
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.number_field.number_field import NumberField
from sage.rings.number_field.number_field import CyclotomicField

QQx = QQ['x']; (x,) = QQx._first_ngens(1)

def bls(k:int):
    """
    BLS curves

    INPUT:
    - `k`: embedding degree multiple of 3

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D
    p(x) field characteristic,
    r(x) subgroup order that has embedding degree k,
    t(x) trace,
    y(x) such that t^2(x) - 4*p(x) = -D*y^2(x)
    D discriminant, D=3
    """
    if (k % 3) != 0 or (k % 18 == 0):
        raise ValueError("Error with BLS k=0 mod 3 and k != 0 mod 18 but k={}={} mod 3 = {} mod 18 given".format(k, k%3, k%18))
    D = 3
    tx = QQx(x+1)
    Phi_k = QQx(cyclotomic_polynomial(k))
    rx = Phi_k
    # 3 | k -> sqrt(-3) does exist mod rx
    # x^(k//3) is a cube root of unity
    if k%6 == 0:
        yx = (tx-2)*(2*x**(k//6)-1)/3
    else: # k = 3 mod 6
        yx = (tx-2)*(2*x**(k//3)+1)/3
    if k != 6:
        yx = yx % rx
    px = (tx**2 + 3*yx**2)/4
    lambx = QQx(x**(k//3)) # the eigenvalue
    px_denom = Integer(lcm([ci.denom() for ci in px.list()]))
    assert ((px+1-tx) % rx) == 0, "Error (px+1-tx)%rx != 0, px={}*({}), tx={}, rx={}, (px+1-tx)={}".format(px_denom, px_denom*px, tx,rx, (px+1-tx).factor())
    cx = (px+1-tx) // rx
    if 3**valuation(k,3) == k: # k is a power of 3
        rx = rx/3
        cx = cx*3
    assert (Phi_k(px) % rx) == 0, "Error k={}, Phi_k(px)%rx != 0, = {}".format(k, Phi_k(px) % rx)
    assert px.is_irreducible(), "Error px is not irreducible, px= {}".format(px.factor())
    assert rx.is_irreducible(), "Error rx is not irreducible, rx= {}".format(rx.factor())

    assert 4*px == (tx**2 + D*yx**2), "Error 4*px != tx^2+D*yx^2, {}*px = {}, tx={}, D={}, yx={}, tx^2+Dyx^2 = {}".format(px_denom, px_denom*px,tx,D,yx,tx**2+D*yx**2)

    return px, rx, tx, yx, D


def fst62(k:int):
    """
    Construction 6.2 in [FST]

    INPUT:
    - `k`: odd embedding degree

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D
    (field characteristic, subgroup order and elliptic curve trace)
    r(x) = Phi_{4k}(x) the 4*k-th cyclotomic polynomial
    t(x) = -x^2+1
    p(x) = (t^2(x) + y^2(x))/4
    y(x) = x^(k+2) + x^k
    D = 1
    """
    if (k % 2) != 1:
        raise ValueError("Error k should be odd, but k={}=0 mod 2".format(k))
    rx = QQx(cyclotomic_polynomial(4*k))
    # x is a (4*k)-th root of unity, x^2 is a (2*k)-th root of unity, and x^4 is a k-th root of unity
    # (x^4)^k = 1
    # (x^2)^k = -1 <=> -(x^2)^k = 1
    # because k is odd, -(x^2)^k = 1 <=> (-x^2)^k = 1 hence (-x^2) is a primitive k-th root of unity
    # 1/sqrt(-1) = x^k
    # (tx-2)/sqrt(-1) = +/- (-x^2-1)*x^k = +/- (x^(k+2) + x^k)
    tx = QQx(-x**2+1)
    px = QQx(x**(2*k+4) + 2*x**(2*k+2) + x**(2*k) + x**4 -2*x**2 + 1)/4
    yx = QQx(x**(k+2) + x**k)
    D = 1
    assert (tx**2 + D*yx**2)/4 == px
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    return px, rx, tx, yx, D

def fst63(k):
    """
    Construction 6.3 in [FST], this is 6.2 modified to work with even k, k=2 mod 4

    INPUT:
    - `k`: even embedding degree, k = 2 mod 4

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D
    (field characteristic, subgroup order and elliptic curve trace, and y(x))
    r(x) = Phi_{2k}(x) the 2*k-th cyclotomic polynomial
    t(x) = x^2+1
    p(x) = (t^2(x) + y^2(x))/4
    y(x) = (x^2-1)*x^(k//2)
    D = 1
    """
    if (k % 4) != 2:
        raise ValueError("Error k should be 2 mod 4 but k={}={} mod 4".format(k, k%4))
    rx = QQx(cyclotomic_polynomial(2*k))
    # zeta_k = x^2, and zeta_k^k = x^(2*k) = 1
    tx = QQx(x**2+1)
    yx = QQx(x**(k//2+2) - x**(k//2))
    # yx = (tx-2)/sqrt(-1) = (x^2-1)/sqrt(-1) and x^(k//2) = sqrt(-1) then yx = (x^2-1)*x^(k//2)
    # px = (tx^2 + yx^2)/4 = (x^2+1)^2 + (x^2-1)^2*x^k = x^4 + 2*x^2 + 1 + (x^4 - 2*x^2 + 1)*x^k
    px = QQx(x**(k+4) - 2*x**(k+2) + x**k + x**4 +2*x**2 + 1)/4
    D = 1
    assert (tx**2 + D*yx**2)/4 == px
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    return px, rx, tx, yx, D

def fst64(k):
    """
    Construction 6.4 in [FST], this is 6.2 modified to work with k=4 mod 8

    INPUT:
    - `k`: embedding degree, k = 4 mod 8

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D
    (field characteristic, subgroup order and elliptic curve trace, and y(x))
    r(x) = Phi_k(x) the k-th cyclotomic polynomial
    t(x) = x+1
    p(x) = (t^2(x) + y^2(x))/4
    y(x) = (x-1)*x^(k//4)
    D = 1
    """
    # if k=0 mod 8 then px is not irreducible
    if (k % 8) != 4:
        raise ValueError("Error k should be 4 mod 8 but k={}={} mod 8".format(k, k%8))
    rx = QQx(cyclotomic_polynomial(k))
    tx = QQx(x+1)
    yx = QQx(x**(k//4+1) - x**(k//4))
    px = QQx(x**(k//2+2) -2*x**(k//2+1) + x**(k//2) + x**2 +2*x +1)/4
    D = 1
    assert (tx**2 + D*yx**2)/4 == px
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    return px, rx, tx, yx, D

def fst65():
    """
    Construction 6.5 that works only with k=10
    """
    k = 10
    rx = QQx(cyclotomic_polynomial(20)) # = x^8 - x^6 + x^4 - x^2 + 1
    tx = QQx(-x**6 + x**4 - x**2 + 2)
    px = QQx((x**12 - x**10 + x**8 - 5*x**6 + 5*x**4 - 4*x**2 + 4)/4)
    yx = QQx(x**5 - x**3)
    cx = QQx(x**4/4)
    D = 1
    assert (tx**2 + D*yx**2)/4 == px
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    return px, rx, tx, yx, D

def fst66(k):
    """
    Construction 6.6 in [FST], with discriminant D=3

    INPUT:
    - `k`: embedding degree, k != 0 mod 18

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D=3
    (field characteristic, subgroup order and elliptic curve trace, and y(x))
    r(x) = some cyclotomic polynomial Phi_l(x) with l a multiple of k
    t(x)
    p(x) = (t^2(x) + 3*y^2(x))/4
    y(x)
    D = 3

    NOTE: there is a problem with the parameters for k=3 mod 18 
    in the paper version (Journal of Crypto)
    we consider the ePrint version (eprint 2006/372)
    """
    FST_JC = False
    if (k % 18) == 0:
        raise ValueError("k=0 mod 18 is not possible")
    D = 3
    if (k % 6) == 1:
        rx = QQx(cyclotomic_polynomial(6*k))
        tx = QQx(-x**(k+1)+x+1)
        # x^k is a 6-th root of unity (1+sqrt(-3))/2 -> 2*x^k-1 = sqrt(-3)
        # 1/sqrt(-3) = (2*x^k-1)/3
        assert (2*x**k-1)**2 % rx == -3
        #yx = QQx((tx-2)*(2*x**k-1)/3) #= (-x**(k+1)+x-1)*(2*x**k-1)/3
        #assert yx == (-x**(k+1)+x-1)*(2*x**k-1)/3
        # yx = (-2*x**(2*k+1) + 2*x**(k+1) - 2*x**k +x**(k+1)-x+1)/3
        #yx = (-2*x**(2*k+1) + 3*x**(k+1) - 2*x**k -x+1)/3
        # after modular reduction, we get yx to be
        yx = QQx((-x**(k+1) + 2*x**k -x-1)/3)
        px = QQx((x+1)**2/3*(x**(2*k)-x**k+1)-x**(2*k+1))
    elif (k % 6) == 2:
        rx = QQx(cyclotomic_polynomial(3*k))
        tx = QQx(x**(k//2+1)-x+1)
        assert (2*x**(k//2)-1)**2 % rx == -3
        #yx = QQx((tx-2) * (2*x**(k//2)-1)/3)
        #assert yx == (x**(k//2+1)-x-1) * (2*x**(k//2)-1)/3
        yx = QQx((x**(k//2+1)+2*x**(k//2)+x-1)/3)
        px = QQx((x-1)**2/3*(x**k-x**(k//2)+1)+x**(k+1))
    elif (k % 6) == 3:
        #if FST_JC:
        #    rx = QQx(cyclotomic_polynomial(2*k))
        #    tx = QQx(-x**(k//3+1)+x+1)
        #    assert (2*x**(k//3)-1)**2 % rx == -3
        #    #yx = QQx((tx-2) * (2*x**(k//3)-1)/3)
        #    yx = QQx((-x**(k//3+1)+2*x**(k//3)-x-1)/3)
        #    px = QQx((x+1)**2/3*(x**(2*k//3)-x**(k//3)+1)-x**(2*k//3+1))
        if (k % 18) == 3:
            rx = QQx(cyclotomic_polynomial(2*k))
            tx = QQx(x**(k//3+1)+1)
            assert (2*x**(k//3)-1)**2 % rx == -3
            #yx = QQx((tx-2) * (2*x**(k//3)-1)/3)
            yx = QQx((-x**(k//3+1)+2*x**(k//3)+2*x-1)/3)
            px = QQx((x**2-x+1)/3*(x**(2*k//3)-x**(k//3)+1)+x**(k//3+1))
        elif (k % 18) == 9 or (k % 18) == 15:
            rx = QQx(cyclotomic_polynomial(2*k))
            tx = QQx(-x**(k//3+1)+x+1)
            assert (2*x**(k//3)-1)**2 % rx == -3
            #yx = QQx((tx-2) * (2*x**(k//3)-1)/3)
            yx = QQx((-x**(k//3+1)+2*x**(k//3)-x-1)/3)
            px = QQx((x+1)**2/3*(x**(2*k//3)-x**(k//3)+1)-x**(2*k//3+1))
    elif (k % 6) == 4:
        rx = QQx(cyclotomic_polynomial(3*k))
        tx = QQx(x**3+1)
        assert (2*x**(k//2)-1)**2 % rx == -3
        yx = QQx((tx-2) * (2*x**(k//2)-1)/3)
        px = QQx((x**3-1)**2/3*(x**k-x**(k//2)+1) + x**3)
    elif (k % 6) == 5:
        rx = QQx(cyclotomic_polynomial(6*k))
        tx = QQx(x**(k+1)+1)
        assert (2*x**k-1)**2 % rx == -3
        #yx = QQx((tx-2)*(2*x**k-1)/3)
        yx = QQx((-x**(k+1)+2*x**k+2*x-1)/3)
        px = QQx((x**2-x+1)/3*(x**(2*k)-x**k+1) + x**(k+1))
    elif (k % 6) == 0:
        rx = QQx(cyclotomic_polynomial(k))
        tx = QQx(x**(k+1)+1)
        assert (2*x**(k//6)-1)**2 % rx == -3
        #yx = QQx((tx-2) * (2*x**(k//6)-1)/3)
        yx = QQx((x-1)*(2*x**(k//6)-1)/3)
        px = QQx((x-1)**2/3*(x**(k//3)-x**(k//6)+1) + x)
    tx = tx % rx
    #yx = yx % rx
    try:
        assert tx**2 - 4*px == -D*yx**2
        assert ((px+1-tx) % rx) == 0
        assert (cyclotomic_polynomial(k)(px) % rx) == 0
        assert px.is_irreducible()
        assert rx.is_irreducible()
    except AssertionError as err:
        print("Error k = {} = {} mod 6 = {} mod 18 D = {}\npx = {}\ntx = {}\nrx = {}\nyx = {}\ntx**2-4*px==-D*yx**2:{}\n((px+1-tx)%rx)==0:{}\n(cyclotomic_polynomial(k)(px)%rx)==0:{}\npx.is_irreducible():{}\nrx.is_irreducible():{}".format(k, k%6, k%18, D, px, tx, rx, yx, tx**2-4*px==-D*yx**2, ((px+1-tx)%rx)==0, (cyclotomic_polynomial(k)(px)%rx)==0, px.is_irreducible(), rx.is_irreducible()))
    return px, rx, tx, yx, D

def fst67(k):
    """
    Construction 6.7 in [FST]

    INPUT:
    - `k`: embedding degree multiple of 3

    RETURN: univariate polynomials px, rx, tx, yx, and discriminant D=2
    (field characteristic, subgroup order and elliptic curve trace)
    r(x) = Phi_{l}(x) the l-th cyclotomic polynomial, l = lcm(8, k)
    t(x) = x^(l/k)+1
    y(x) = (1-x^(l/k)) * x^(l/24) * (x^(l/6) + x^(l/12) - 1)/2
    p(x) = (t^2(x) + y^2(x))/4
    D = 1
    """
    if (k%3) != 0:
        print("Construction 6.7, 3 | k required but k={}={} mod 3 given".format(k,k%3))
        return
    if k==3:
        print("Construction 6.7, k=3 does not work. Please choose a larger k=0 mod 3.")
        return
    l = lcm(8,k)
    rx = QQx(cyclotomic_polynomial(l))
    tx = QQx(x**(l // k) + 1)
    px = QQx(((x**(l//k)+1)**2 + (1-x**(l//k))**2*(x**(5*l//24) + x**(l//8) - x**(l//24))**2/2)/4)
    tx = QQx(tx % rx)
    yx = QQx((1-x**(l//k))*(x**(5*l//24)+x**(l//8)-x**(l//24))/2)
    D = 2
    assert tx**2 - 4*px == -D*yx**2
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    return px, rx, tx, yx, D

def bn():
    """
    The well-known Barreto-Naehrig curves
    """
    px = QQx(36*x**4+36*x**3+24*x**2+6*x+1)
    rx = QQx(36*x**4+36*x**3+18*x**2+6*x+1)
    tx = QQx(6*x**2+1)
    yx = QQx(6*x**2 + 4*x + 1)
    D = 3
    k = 12
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    assert tx**2 - 4*px == -D*yx**2
    return px, rx, tx, yx, D

def fst68():
    return bn()

def fst69():
    tx = QQx(-4*x**3)
    rx = QQx(4*x**4+4*x**3+2*x**2+2*x+1)
    px = QQx((16*x**6+8*x**4+4*x**3+4*x**2+4*x+1)/3)
    yx = QQx((4*x**3 + 4*x + 2)/3)
    D = 3
    k = 4
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    assert tx**2 - 4*px == -D*yx**2
    return px, rx, tx, yx, D

def fst610():
    k = 8
    D = 1
    rx = QQx(9*x**4 + 12*x**3 + 8*x**2 + 4*x + 1)
    tx = QQx(-9*x**3 - 3*x**2 - 2*x)
    px = QQx((81*x**6 + 54*x**5 + 45*x**4 + 12*x**3 + 13*x**2 + 6*x + 1)/4)
    yx = QQx(-3*x-1)
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    assert tx**2 - 4*px == -D*yx**2
    return px, rx, tx, yx, D

def kss(k):
    if k==8:
        px = QQx((x**6+2*x**5-3*x**4+8*x**3-15*x**2-82*x+125)/180)
        rx = QQx((x**4-8*x**2+25)/450)
        tx = QQx((2*x**3-11*x+15)/15)
        cx = QQx((x**2 + 2*x + 5)*5/2)
        yx = QQx((x**3 + 5*x**2 + 2*x - 20)/15)
        D = 1
        # u = 5, 25 mod 30
    elif k==16:
        px = QQx((x**10 + 2*x**9 + 5*x**8 + 48*x**6 + 152*x**5 + 240*x**4 + 625*x**2 + 2398*x + 3125)/980)
        rx = QQx((x**8 + 48*x**4 + 625)/61250)
        tx = QQx((2*x**5 + 41*x + 35)/35)
        cx = QQx((x**2 + 2*x + 5)*125/2)
        yx = QQx((x**5 + 5*x**4 + 38*x + 120)/35)
        D = 1
        # u = 25, 45 mod 70
    elif k==18:
        px = QQx((x**8 + 5*x**7 + 7*x**6 + 37*x**5 + 188*x**4 + 259*x**3 + 343*x**2 + 1763*x + 2401)/21)
        rx = QQx((x**6 + 37*x**3 + 343)/343)
        tx = QQx((x**4+16*x+7)/7)
        cx = QQx((x**2 + 5*x + 7)*49/3)
        yx = QQx((5*x**4 + 14*x**3 + 94*x + 259)/21)
        D = 3
        # u = 14 mod 21
    elif k==32:
        px = QQx((x**18-6*x**17+13*x**16+57120*x**10-344632*x**9 + 742560*x**8 + 815730721*x**2 - 4948305594*x + 10604499373)/2970292)
        rx = QQx((x**16+57120*x**8+815730721)/93190709028482)
        tx = QQx((-2*x**9 - 56403*x + 3107)/3107)
        yx = QQx((3*x**9 - 13*x**8 + 86158*x - 371280)/3107)
        cx = QQx((x**2 - 6*x + 13)*62748517/2)
        D = 1
    elif k==36:
        px = QQx((x**14-4*x**13+7*x**12+683*x**8-2510*x**7 + 4781*x**6 + 117649*x**2 - 386569*x + 823543)/28749)
        rx = QQx((x**12 + 683*x**6 + 117649)/161061481)
        tx = QQx((2*x**7+757*x+259)/259)
        yx = QQx((4*x**7 - 14*x**6 + 1255*x - 4781)/777)
        cx = QQx((x**2 - 4*x + 7)*16807/3)
        D = 3
    elif k==40:
        px = QQx((x**22 - 2*x**21 + 5*x**20 + 6232*x**12 - 10568*x**11 + 31160*x**10 + 9765625*x**2 - 13398638*x + 48828125)/1123380)
        rx = QQx((x**16 + 8*x**14 + 39*x**12 + 112*x**10 - 79*x**8 + 2800*x**6 + 24375*x**4 + 125000*x**2 + 390625)/2437890625)
        tx = QQx((2*x**11 + 6469*x + 1185)/1185)
        yx = QQx((x**11 - 5*x**10 + 2642*x - 15580)/1185)
        cx = QQx((x**2 - 2*x + 5) * (x**4 - 8*x**2 + 25)*78125/36)
        D = 1
    elif k==54:
        px = 1+3*x+3*x**2+3**5*x**9+3**5*x**10+3**6*x**10+3**6*x**11+3**9*x**18+3**10*x**19+3**10*x**20
        rx = 1+3**5*x**9+3**9*x**18
        tx = 1+3**5*x**10
        yx = 243*x**10 + 162*x**9 + 2*x + 1
        cx = 1+3*x+3*x**2
        D = 3
    assert ((px+1-tx) % rx) == 0
    assert (cyclotomic_polynomial(k)(px) % rx) == 0
    assert px.is_irreducible()
    assert rx.is_irreducible()
    assert tx**2 - 4*px == -D*yx**2
    return px, rx, tx, yx, D

def fst611():
    return kss(16)
def fst612():
    return kss(18)
def fst613():
    return kss(32)
def fst614():
    return kss(36)
def fst615():
    return kss(40)

def test_fst62():
    print("fst62")
    for k in range(3, 100, 2):
        px, rx, tx, yx, D = fst62(k)
    print("fst62 ok for all k = 1 mod 2, 3 <= k <= 100")

def test_fst63():
    print("fst63")
    for k in range(2, 100, 4):
        px, rx, tx, yx, D = fst63(k)
    print("fst63 ok for all k = 2 mod 4, 2 <= k <= 100")

def test_fst64():
    print("fst64")
    for k in range(4, 100, 8):
        px, rx, tx, yx, D = fst64(k)
    print("fst64 ok for all k = 4 mod 8, 4 <= k <= 100")

def test_fst65():
    print("fst65")
    px, rx, tx, yx, D = fst65()
    print("fst65 ok (k=10)")

def test_fst66():
    print("fst66")
    for k0 in [7,8,9,10,11,12]:
        for k in range(k0, 100, 6):
            if (k % 18) == 0:
                continue
            px, rx, tx, yx, D = fst66(k)
    print("fst66 ok for all k != 0 mod 18, 7 <= k <= 100")

def test_fst67():
    print("fst67")
    for k in range(6, 100, 3):
        px, rx, tx, yx, D = fst67(k)
    print("fst67 ok for all k = 0 mod 3, 6 <= k <= 100")

def test_fst68():
    print("fst68 (bn)")
    px, rx, tx, yx, D = fst68()
    print("fst68 (bn) ok, k = 12")

def test_fst69():
    print("fst69")
    px, rx, tx, yx, D = fst69()
    print("fst69 ok (k = 4)")

def test_fst610():
    print("fst610")
    px, rx, tx, yx, D = fst610()
    print("fst610 ok (k = 10)")

def test_fst611():
    print("fst611")
    px, rx, tx, yx, D = fst611()
    print("fst611 ok (kss k = 16)")

def test_fst612():
    print("fst612")
    px, rx, tx, yx, D = fst612()
    print("fst612 ok (kss k = 18)")

def test_fst613():
    print("fst613")
    px, rx, tx, yx, D = fst613()
    print("fst613 ok (kss k = 32)")

def test_fst614():
    print("fst614")
    px, rx, tx, yx, D = fst614()
    print("fst614 ok (kss k = 36)")

def test_fst615():
    print("fst615")
    px, rx, tx, yx, D = fst615()
    print("fst615 ok (kss k = 40)")

def test_kss():
    print("kss k=8, k=16 (fst611) k=18 (fst612) k=32 (fst613) k=36 (fst614) k=40 (fst615)")
    for k in [8, 16, 18, 32, 36, 40, 54]:
        px, rx, tx, yx, D = kss(k)
    print("kss ok for all k in [8,16,18,32,36,40,54]")

def test_yx_fst62(verbose=False):
    print("fst62")
    for k in range(3, 100, 2):
        px, rx, tx, yx, D = fst62(k)
        cx = (px+1-tx)//rx
        ax = QQx(sum([x**i for i in range(0, 2*k-1, 2)])) # 1 + x^2 + x^4 + ... + x^{2k-2}
        px2 = px(2*x)
        px1 = px(2*x+1)
        d2 = lcm([pi.denom() for pi in px2.list()])
        d1 = lcm([pi.denom() for pi in px1.list()])
        c2 = gcd([pi.numer() for pi in (d2*px2).list()])
        c1 = gcd([pi.numer() for pi in (d1*px1).list()])
        assert d2 == 4 and d1 == 1
        assert c2 == 1 and c1 == 1
        if verbose:
            print("k = {} cx = {} = {}".format(k, cx, cx.factor()))
            print("lcm(denom(px(2x))) = {}\nlcm(denom(px(2x+1))) = {}".format(d2, d1))
            print("px-1 = {}".format((px-1).factor()))
            print("ax = {}".format(ax))
            print("gcd(px-1, cx) = {}".format(gcd(px-1, cx)))
            print("gcd(yx, cx)   = {}".format(gcd(yx, cx)))
        c0x = (x**2+1)/2
        ccx = (x**2+1)**3/4
        assert (cx % ccx) == 0
        assert ((px-1) % c0x) == 0
        assert (px-1) == (x**4-1)*(x**(2*k) + 2*ax + 1)/4
        assert (yx % c0x) == 0
    print("checked that cx has factor (x^2+1)^3/4 and (x^2+1)/2 | (px-1), (x^2+1)/2 | yx")
    print("checked that px(2*x) has denominator 4 (not integer) and px(2*x+1) has denominator 1 (is integer)")

def test_yx_fst63(verbose=False):
    print("fst63")
    for k in range(2, 100, 4):
        px, rx, tx, yx, D = fst63(k)
        cx = (px+1-tx)//rx
        px2 = px(2*x)
        px1 = px(2*x+1)
        d2 = lcm([pi.denom() for pi in px2.list()])
        d1 = lcm([pi.denom() for pi in px1.list()])
        c2 = gcd([pi.numer() for pi in (d2*px2).list()])
        c1 = gcd([pi.numer() for pi in (d1*px1).list()])
        assert d2 == 4 and d1 == 1
        assert c2 == 1 and c1 == 1
        if verbose:
            print("k = {} cx = {} = {}".format(k, cx, cx.factor()))
            print("rx = {}".format(rx.factor()))
            print("px-1 = {}".format((px-1).factor()))
            print("gcd(px-1, cx) = {}".format(gcd(px-1, cx)))
            print("gcd(yx, cx)   = {}".format(gcd(yx, cx)))
        c0x = (x**2-1)/2
        if k == 2:
            ccx = (x**2-1)**2/4
        else:
            ccx = (x**2-1)**2*(x**2+1)/4
        assert (cx % ccx) == 0
        assert (px-1) == (x**2-1)*(x**k*(x**2-1) + x**2 + 3)/4
        assert ((px-1) % c0x) == 0
        assert (yx % c0x) == 0
    print("checked that cx has factor (x^2-1)^2/4 for k=2 and (x^2-1)^2*(x^2+1)/4 for k > 2, and (x^2-1)/2 | (px-1), (x^2-1)/2 | yx")
    print("checked that px(2*x) has denominator 4 (not integer) and px(2*x+1) has denominator 1 (is integer)")

def test_yx_fst64(verbose=False):
    print("fst64")
    print("k=4")
    px, rx, tx, yx, D = fst64(4)
    print("px= {}\npx-1 = {}\npx+1-tx = {}\nrx={}\ntx={}\nyx={}\n".format(px.factor(), (px-1).factor(), (px+1-tx).factor(), rx.factor(), tx, yx.factor()))
    for k in range(4, 100, 8):
        px, rx, tx, yx, D = fst64(k)
        px2 = px(2*x)
        px1 = px(2*x+1)
        d2 = lcm([pi.denom() for pi in px2.list()])
        d1 = lcm([pi.denom() for pi in px1.list()])
        c2 = gcd([pi.numer() for pi in (d2*px2).list()])
        c1 = gcd([pi.numer() for pi in (d1*px1).list()])
        assert d2 == 4 and d1 == 1
        assert c2 == 1 and c1 == 1
        cx = (px+1-tx)//rx
        if verbose:
            print("k = {} cx = {} = {}".format(k, cx, cx.factor()))
            print("gcd(px-1, cx) = {}".format(gcd(px-1, cx)))
            print("gcd(yx, cx)   = {}".format(gcd(yx, cx)))
        c0x = (x-1)/2
        if k == 4:
            ccx = (x-1)**2/4
        else:
            ccx = (x-1)**2*(x**2+1)/4
        assert (cx % ccx) == 0
        assert ((px-1) % c0x) == 0
        assert (yx % c0x) == 0
    print("checked that cx has factor (x-1)^2/4, and (x-1)/2 | (px-1), (x-1)/2 | yx")
    print("checked that px(2*x) has denominator 4 (not integer) and px(2*x+1) has denominator 1 (is integer)")

def test_yx_fst65(verbose=False):
    print("fst65")
    k = 10
    px, rx, tx, yx, D = fst65()
    cx = (px+1-tx)//rx
    if verbose:
        print("k = {} cx = {} = {}".format(k, cx, cx.factor()))
        print("gcd(px-1, cx) = {}".format(gcd(px-1, cx)))
        print("gcd(yx, cx)   = {}".format(gcd(yx, cx)))
    c0x = x**2/2
    ccx = x**4/4
    assert (cx % ccx) == 0
    assert ((px-1) % c0x) == 0
    assert (yx % c0x) == 0
    print("checked that cx has factor (x^2/2)^2 for k=10 and x^2/2 | (px-1), x^2/2 | yx")

def test_yx_fst66(verbose=False):
    print("fst66")
    for k0 in [7,8,9,10,11,6]:
        if verbose:
            print("")
        for k in range(k0, 100, 6):
            if (k % 18) == 0:
                continue
            px, rx, tx, yx, D = fst66(k)
            px0 = px(3*x)
            px1 = px(3*x+1)
            px2 = px(3*x+2)
            d0 = lcm([pi.denom() for pi in px0.list()])
            d1 = lcm([pi.denom() for pi in px1.list()])
            d2 = lcm([pi.denom() for pi in px2.list()])
            c0 = gcd([pi.numer() for pi in (d0*px0).list()])
            c1 = gcd([pi.numer() for pi in (d1*px1).list()])
            c2 = gcd([pi.numer() for pi in (d2*px2).list()])
            cx = (px+1-tx)//rx
            if verbose:
                print("k = {} = {} mod 6 = {} mod 18".format(k, k % 6, k % 18))
                print("px(3*x)   = {}/{}*Px where Px in Z[x]".format(c0,d0))
                print("px(3*x+1) = {}/{}*Px where Px in Z[x]".format(c1,d1))
                print("px(3*x+2) = {}/{}*Px where Px in Z[x]".format(c2,d2))
                print("cx = {} = {}".format(cx, cx.factor()))
                print("gcd(px-1, cx) = {}".format(gcd(px-1, cx)))
                if (k % 6) == 3:
                    print("px-1 = {}".format((px-1).factor()))
                    print("px+1-tx = {}".format((px+1-tx).factor()))
                print("gcd(yx, cx)   = {}".format(gcd(yx, cx)))
                print("yx = ({})/3 \n   = {}".format(3*yx, yx.factor()))
            if (k % 6) == 1:
                c0x = (x**2-x+1)/3
                ccx = (x**2-x+1)**2/3
                assert (cx % ccx) == 0
                assert ((px-1) % c0x) == 0
                assert (yx % c0x) == 0
                assert px-1 == (x**2-x+1)*(x**(2*k)-x**k+1)/3 - (x**(k+1)-x+1)
                assert d0 == 3 and d1 == 3 and d2 == 1
                assert c0 == 1 and c1 == 1 and c2 == 1
            elif (k % 6) == 2:
                ccx = (x**2+x+1)/3
                assert (cx % ccx) == 0
                # no cofactor clearing simplification in this case
                assert (gcd(cx, yx)).degree() == 0
                assert (gcd(cx, px-1)).degree() == 0
                if (k % 12) == 8:
                    assert d0 == 3 and d1 == 1 and d2 == 3
                    assert c0 == 1 and c1 == 1 and c2 == 1
                elif (k % 12) == 2:
                    assert d0 == 3 and d1 == 1 and d2 == 1
                    assert c0 == 1 and c1 == 1 and c2 == 3                    
            elif (k % 18) == 9:
                assert d0 == 3 and d1 == 3 and d2 == 1
                assert c0 == 1 and c1 == 1 and c2 == 1
                assert (px+1-tx) == (x**2-x+1)*(x**(2*k//3) - x**(k//3)+1)/3
                ccx = (x**2-x+1)/3
                assert (cx % ccx) == 0
                assert (gcd(cx, yx)).degree() == 0
                assert (gcd(cx, px-1)).degree() == 0
            elif (k % 18) == 15 or (k % 18) == 3:
                assert d0 == 3 and d1 == 3 and d2 == 1
                assert c0 == 1 and c1 == 1 and c2 == 1
                if (k % 18) == 15:
                    assert (px+1-tx) == (x**2-x+1)*(x**(2*k//3) - x**(k//3)+1)/3
                ccx = (x**2-x+1)**2/3
                assert (cx % ccx) == 0
                assert (gcd(cx, yx)).degree() == 0
                assert (gcd(cx, px-1)).degree() == 0
            elif (k % 6) == 4:
                ccx = (x**3-1)**2/3
                c0x = (x**3-1)/3
                assert (cx % ccx) == 0
                assert ((px-1) % c0x) == 0
                assert (yx % c0x) == 0
                if (k % 12) == 4:
                    assert d0 == 3 and d1 == 1 and d2 == 3
                    assert c0 == 1 and c1 == 1 and c2 == 1
                elif (k % 12) == 10:
                    assert d0 == 3 and d1 == 1 and d2 == 1
                    assert c0 == 1 and c1 == 1 and c2 == 3                    
            elif (k % 6) == 5:
                ccx = (x**2-x+1)**2/3
                c0x = (x**2-x+1)/3
                assert (cx % ccx) == 0
                assert ((px-1) % c0x) == 0
                assert (yx % c0x) == 0
                assert d0 == 3 and d1 == 3 and d2 == 1
                assert c0 == 1 and c1 == 1 and c2 == 1
            elif (k % 6) == 0:
                ccx = (x-1)**2/3
                c0x = (x-1)/3
                assert (cx % ccx) == 0
                assert ((px-1) % c0x) == 0
                assert (yx % c0x) == 0
                if (k % 12) == 0:
                    assert d0 == 3 and d1 == 1 and d2 == 3
                    assert c0 == 1 and c1 == 1 and c2 == 1
                elif (k % 12) == 6:
                    assert d0 == 3 and d1 == 1 and d2 == 1
                    assert c0 == 1 and c1 == 1 and c2 == 3
    print("checked cofactors for 6.6")
                
def test_yx_fst67(verbose=False):
    print("fst67")
    for k in range(6, 100, 3):
        px, rx, tx, yx, D = fst67(k)
        px0 = px(4*x)
        px1 = px(4*x+1)
        px2 = px(4*x+2)
        px3 = px(4*x+3)
        d0 = lcm([pi.denom() for pi in px0.list()])
        d1 = lcm([pi.denom() for pi in px1.list()])
        d2 = lcm([pi.denom() for pi in px2.list()])
        d3 = lcm([pi.denom() for pi in px3.list()])
        c0 = gcd([pi.numer() for pi in (d0*px0).list()])
        c1 = gcd([pi.numer() for pi in (d1*px1).list()])
        c2 = gcd([pi.numer() for pi in (d2*px2).list()])
        c3 = gcd([pi.numer() for pi in (d3*px3).list()])
        cx = (px+1-tx)//rx
        if verbose:
            print("k = {} cx = {} = {}".format(k, cx, cx.factor()))
            print("px(4*x)   = {}/{}*Px where Px in Z[x]".format(c0,d0))
            print("px(4*x+1) = {}/{}*Px where Px in Z[x]".format(c1,d1))
            print("px(4*x+2) = {}/{}*Px where Px in Z[x]".format(c2,d2))
            print("px(4*x+3) = {}/{}*Px where Px in Z[x]".format(c3,d3))
            gcp = gcd(px-1, cx)
            gcy = gcd(yx, cx)
            print("gcd(px-1, cx) = {} = {}".format(gcp, gcp.factor()))
            print("gcd(yx, cx)   = {} = {}".format(gcy, gcy.factor()))
        if k==3:
            c0x = 1
            ccx = 1
            continue
        l = lcm(k,8)
        ccx = (x**(l//k)-1)**2/8
        c0x = (x**(l//k)-1)/8
        assert (cx % ccx) == 0
        assert ((px-1) % c0x) == 0
        assert (yx % c0x) == 0
        if (k % 24) == 0:
            assert d0 == 4 and d1 == 1 and d2 == 4 and d3 == 2
            assert c0 == 1 and c1 == 1 and c2 == 1 and c3 == 1
        else:
            assert d0 == 4 and d1 == 1 and d2 == 4 and d3 == 1
            assert c0 == 1 and c1 == 1 and c2 == 1 and c3 == 1
    print("checked cofactors 6.7")

def test_yx_kss(verbose=False):
    print("kss")
    for k in [8,16,18,32,36,40,54]:
        px, rx, tx, yx, D = kss(k)
        cx = (px+1-tx)//rx
        gcp = gcd(px-1, cx)
        gcy = gcd(yx, cx)
        assert gcp.degree() == 0
        assert gcy.degree() == 0
        if verbose:
            print("k = {} cx = {}\n   = {}".format(k, cx, cx.factor()))
            print("gcd(px-1, cx) = {} = {}".format(gcp, gcp.factor()))
            print("gcd(yx, cx)   = {} = {}".format(gcy, gcy.factor()))
    print("checked that there is no faster co-factor clearing on KSS curves")

######################################################################
## second part: G2 cofactor clearing
######################################################################
def poly_cofactor_twist_g1_g2(k: int, px, rx, tx, cx, yx, D):
    """
    Compute the curve co-factors for G2 and the twists of G1, G2

    INPUT:
    - `k`: embedding degree
    - `px`: polynomial of the prime field characteristic
    - `rx`: polynomial of the prime subgroup order
    - `tx`: polynomial of the trace
    - `cx`: cofactor of G1 s.t. rx*cx = px+1-tx
    - `yx`: such that tx^2 - 4*px = -D*yx^2
    """
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    twx = px+1+tx
    D = abs(D)
    # does it factor?
    if (D == 3) and (k % 6) == 0:
        # sextic twist
        d = 6
    elif (D == 3) and (k % 3) == 0:
        # cubic twist
        d = 3
    elif ((D == 1) or (D==4)) and (k % 4) == 0:
        # quartic twist
        d = 4
    elif (k % 2) == 0:
        # quadratic twist
        d = 2
    else:
        # no twist
        d = 1
    k1 = k // d
    # compute the curve order up to k1 before computing the d-twist order
    t1 = tx
    # (p+1-t)*(p+1+t) = p^2 + 1 + 2*p - t^2 = p^2 + 1 - (t^2 - 2*p)
    t2 = tx**2 - 2*px
    # tk = t1*t_{k-1} -p*t_{k-2}
    i = 3
    t_im1 = t2 # t_{i-1}
    t_im2 = t1 # t_{i-2}
    while i <= k1:
        t_i = t1*t_im1 - px*t_im2
        t_im2 = t_im1
        t_im1 = t_i
        i = i+1
    if k1 == 1:
        tx_k1 = t1
    elif k1 == 2:
        tx_k1 = t2
    else:
        tx_k1 = t_i
    px_k1 = px**k1
    if d==3 or d==6 or d==4:
        yx_k1_square = (tx_k1**2 - 4*px_k1)/(-D)
        lc = yx_k1_square.leading_coefficient()
        assert lc.is_square()
        yx_k1_square_monic = yx_k1_square / lc
        yx_k1_factors = yx_k1_square_monic.factor()
        yx_k1 = lc.sqrt()
        for fa, ee in yx_k1_factors:
            assert (ee % 2) == 0
            yx_k1 = yx_k1 * fa**(ee//2)
        assert yx_k1**2 == yx_k1_square
    else:
        yx_k1 = 1
    if d==3 or d==6:
        if d==6:
            E2_order = px_k1+1-(-3*yx_k1+tx_k1)/2
            E2_order_= px_k1+1-( 3*yx_k1+tx_k1)/2
            g2twx = px_k1+1+(-3*yx_k1+tx_k1)/2
            g2twx_= px_k1+1+( 3*yx_k1+tx_k1)/2
        elif d==3:
            E2_order = px_k1+1-(-3*yx_k1-tx_k1)/2
            E2_order_= px_k1+1-( 3*yx_k1-tx_k1)/2
            g2twx = px_k1+1+(-3*yx_k1-tx_k1)/2
            g2twx_= px_k1+1+( 3*yx_k1-tx_k1)/2
        if (E2_order % rx) != 0 and (E2_order_ % rx) == 0:
            E2_order = E2_order_
            g2twx = g2twx_
    elif d==4:
        if D==1:
            E2_order = px_k1 + 1 + yx_k1
            g2twx = px_k1 + 1 - yx_k1 # quadratic twist of G2
        elif D==4:
            E2_order = px_k1 + 1 + 2*yx_k1
            g2twx = px_k1 + 1 - 2*yx_k1 # quadratic twist of G2
        if (E2_order % rx) != 0 and (g2twx % rx) == 0:
            E2_order, g2twx = g2twx, E2_order
    elif d == 2:
        E2_order = px_k1 + 1 + tx_k1
        g2twx = px_k1 + 1 - tx_k1 # quadratic twist of G2
    else: # d==1
        assert d==1
        E2_order = px_k1 + 1 - tx_k1
        g2twx = px_k1 + 1 + tx_k1 # quadratic twist of G2

    assert (E2_order % rx) == 0
    if d > 1:
        g2cx = E2_order // rx # irreducible
    else:
        g2cx = E2_order // rx**2 # irreducible
    # do cx, twx, g2cx, g2twx factor?
    polys_cofact_twists = [cx, twx, g2cx, g2twx]
    label_factors = ["cx", "twx", "g2cx", "g2twx"]
    small_cofactors = [1, 1, 1, 1]
    return twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors

def compute_corner_cases(k, px, rx, tx, yx, cx, D, lambdax):
    print("k={}".format(k))
    assert (tx**2 - 4*px) == -D*yx**2
    assert (px+1-tx) % rx == 0
    twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors = poly_cofactor_twist_g1_g2(k, px, rx, tx, cx, yx, D)
    # g2cx is the cofactor of G2
    # compute the denominator of g2cx
    d = lcm([ZZ(gi.denom()) for gi in g2cx.list()])
    # compute the XGCD of X^2 -t(x)*X + p(x) at X, with G2cx
    print("c2 = {}".format(g2cx.factor()))

    Chix = lambdax**2 -tx*lambdax + px
    assert (Chix % rx) == 0

    gx, ax, bx = xgcd(Chix, g2cx)
    ax*Chix + bx*g2cx == gx
    da = lcm([ZZ(gi.denom()) for gi in ax.list()]) # this is 3^3 * 1038721 for BLS24, 3*181 for BLS12
    db = lcm([ZZ(gi.denom()) for gi in bx.list()]) # this is 1038721 for BLS24
    L = lcm(da, db)
    print("L = {}".format(L.factor()))
    print("found: lcm(denominators of ax and bx in the XGCD of g2cx and X^2-t(x)*X+p(x))  = {}".format(L.factor()))
    Ca = gcd([ZZ(ri.numer()) for ri in (da*ax).list()])
    Cb = gcd([ZZ(ri.numer()) for ri in (db*bx).list()])
    print("found: gcd of coefficients of ax*{} is {} and bx*{} is {}".format(da, Ca, db, Cb))
    res = (Chix.resultant(g2cx))
    print("alternatively, the resultant of g2cx and X^2-t(x)*X+p(x) is {}".format(res.factor()))
    for l, _ in L.factor():
        print("considering prime factor l = {}".format(l))
        if (res.denominator() % l) == 0:
            print("l={} divides the denominator of the resultant, skipping this l".format(l))
            continue
        #l = 1038721
        Fl = GF(l)
        roots_Px_with_ei = Chix.roots(Fl)
        roots_Px = [ri for ri,ei in roots_Px_with_ei]
        shared_roots = []
        for ri,ei in g2cx.roots(Fl):
            print("{} (with multiplicity {}) is a root of g2cx mod l={}, is also a root of Px=x^2-tx*x+px: {}".format(ri, ei,l, ri in roots_Px))
            if ri in roots_Px:
                shared_roots.append(ZZ(ri))
        print("shared roots are {}".format(shared_roots))
        for r0 in shared_roots:
            for poly_x, poly_str in [(rx, "rx"), (px, "px"), (g2cx, "g2cx"), (cx, "cx")]:
                Rx = poly_x(l*x+r0)
                dr = lcm([ZZ(ri.denom()) for ri in Rx.list()])
                content = gcd([ZZ(ri.numer()) for ri in (dr*Rx).list()])
                print("{}({}*x + {}) = {}/{} * (some polynomial with integer coefficients)".format(poly_str, l, r0, content, dr))


if __name__ == "__main__":
    print("Freeman Scott Teske families")

    print("test the families")
    test_fst62()
    test_fst63()
    test_fst64()
    test_fst65()
    test_fst66()
    test_fst67()
    test_fst68()
    test_fst69()
    test_fst610()
    test_fst611()
    test_fst612()
    test_fst613()
    test_fst614()
    test_fst615()
    test_kss() # includes fst611, fst612, fst613, fst614, fst615

    print("first part: check the cofactors for G1")
    test_yx_fst62()
    test_yx_fst63()
    test_yx_fst64()
    test_yx_fst65()
    test_yx_fst66()
    test_yx_fst67()
    test_yx_kss()

    print("second part: compute the cofactors of G2")
        # BN
    k=12
    px, rx, tx, yx, D = bn()
    cx = (px+1-tx)//rx
    twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors = poly_cofactor_twist_g1_g2(k, px, rx, tx, cx, yx, D)
    print("BN")
    print("c2x = {}".format(g2cx))
    
    # BLS
    for k in [12,15,24,27,48]:
        px, rx, tx, yx, D = bls(k)
        cx = (px+1-tx)//rx
        twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors = poly_cofactor_twist_g1_g2(k, px, rx, tx, cx, yx, D)
        print("BLS-{}".format(k))
        print("c2x = {}".format(g2cx.factor()))

    # 6.6
    for k in [13,19,9,15,21,27]:
        px, rx, tx, yx, D = fst66(k)
        cx = (px+1-tx)//rx
        twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors = poly_cofactor_twist_g1_g2(k, px, rx, tx, cx, yx, D)
        print("FST66-k{}".format(k))
        print("c2x = {}".format(g2cx.factor()))

    # KSS
    for k in [8,16,18,32,36,40,54]:
        px, rx, tx, yx, D = kss(k)
        cx = (px+1-tx)//rx
        twx, g2cx, g2twx, polys_cofact_twists, label_factors, small_cofactors = poly_cofactor_twist_g1_g2(k, px, rx, tx, cx, yx, D)
        print("KSS-{}".format(k))
        print("c2x = {}".format(g2cx.factor()))

    print("compute the exceptional seeds of BN, BLS12, BLS24")
    for k in [12, 24]:
        print("BLS-{}".format(k))
        px, rx, tx, yx, D = bls(k)
        cx = (px+1-tx)//rx
        lambdax = x
        compute_corner_cases(k, px, rx, tx, yx, cx, D, lambdax)
        print("")
    # BN
    print("BN")
    k=12
    px, rx, tx, yx, D = bn()
    cx = QQx(1)
    lambdax = 6*x**2
    compute_corner_cases(k, px, rx, tx, yx, cx, D, lambdax)

    print("done")
